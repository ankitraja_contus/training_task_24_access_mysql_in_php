# Requirement:Access mysql from php container.
   Step1:Create a html file

<!DOCTYPE html>
<html>
<head>
 <title>Form site</title>
</head>
<body>
<form method="post" action="connect.php">
Username : <input type="text" name="username"><br><br>
Password : <input type="password" name="password"><br><br>
<input type="submit" value="Submit">
</form>
</body>
</html>

# Step2:Create php file


<?php
 $username = filter_input(INPUT_POST, 'username');
 $password = filter_input(INPUT_POST, 'password');
 if (!empty($username)){
if (!empty($password)){
$host = "test1_db";
$dbusername = "user";
$dbpassword = "raj";
$dbname = "Data";
// Create connection
$conn = new mysqli ($host, $dbusername, $dbpassword, $dbname);
if (mysqli_connect_error()){
die('Connect Error ('. mysqli_connect_errno() .') '
. mysqli_connect_error());
}
else{
$sql = "INSERT INTO account (username, password)
values ('$username','$password')";
if ($conn->query($sql)){
echo "New record is inserted sucessfully";
}
else{
echo "Error: ". $sql ."
". $conn->error;
}
$conn->close();
}
}
else{
echo "Password should not be empty";
die();
}
}
else{
echo "Username should not be empty";
die();
}
?>

# Step3: Create Dockerfile

FROM php:7.2-apache
COPY a.html /var/www/html/
COPY connect.php /var/www/html/
RUN docker-php-ext-install mysqli

# Step4:Create stack file

version: '3'
services:
  db:
    image: mysql:5.6
    environment:
      MYSQL_ROOT_PASSWORD: ankit
      MYSQL_DATABASE: Data
      MYSQL_USER: user
      MYSQL_PASSWORD: raj
    networks:
      - my-overlay     
  apache-php:
    image: apache-php
    ports:
      - 8087:80
    networks:
      - my-overlay
networks:
  my-overlay:



docker stack deploy -c stack.yml test

# Step5:login into container and login into user

docker exec -it d966b0def297 sh

    mysql -u user -p


    mysql> show databases;

    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | Data               |
    +--------------------+


    mysql> use Data
    Reading table information for completion of table and column names
    You can turn off this feature to get a quicker startup with -A

    Database changed
    mysql> show tables;
    +----------------+
    | Tables_in_Data |
    +----------------+
    | account        |
    +----------------+
    1 row in set (0.00 sec)


    mysql> show tables;
    +----------------+
    | Tables_in_Data |
    +----------------+
    | account        |
    +----------------+
    1 row in set (0.00 sec)

    mysql> select * from account;
    +----------+----------+
    | username | password |
    +----------+----------+
    | ankit    | raj      |
    | ankit    | addg     |
    | ankit    | raj      |
    | ankit    | adfvs    |
    | ankit    | adfvs    |
    | ankit    | adfvs    |
    +----------+----------+
    6 rows in set (0.00 sec)


